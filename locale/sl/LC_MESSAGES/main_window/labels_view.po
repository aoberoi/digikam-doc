# SOME DESCRIPTIVE TITLE.
# Copyright (C) licensed under the  <a href="https://spdx.org/licenses/GFDL-1.2-or-later.html">licensed under the terms of the GNU Free Documentation License 1.2+</a> unless stated otherwise
# This file is distributed under the same license as the Digikam Manual package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: Digikam Manual 8.1.0\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-07-09 00:48+0000\n"
"PO-Revision-Date: 2023-07-02 18:02+0200\n"
"Last-Translator: Matjaž Jeran <matjaz.jeran@amis.net>\n"
"Language-Team: Slovenian <kde-i18n-doc@kde.org>\n"
"Language: sl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n%100==1 ? 1 : n%100==2 ? 2 : n%100==3 || n"
"%100==4 ? 3 : 0);\n"
"X-Generator: Poedit 3.3.1\n"

#: ../../main_window/labels_view.rst:1
msgid "digiKam Main Window Labels View"
msgstr "digiKam pogled oznak glavnega okna"

#: ../../main_window/labels_view.rst:1
msgid ""
"digiKam, documentation, user manual, photo management, open source, free, "
"learn, easy, labels, rating, colors, pick"
msgstr ""
"digiKam, dokumentacija, uporabniški priročnik, upravljanje fotografij, "
"odprta koda, brezplačno, učenje, enostavno, oznake, ocena, barve, izbira"

#: ../../main_window/labels_view.rst:14
msgid "Labels View"
msgstr "Pogled oznak"

#: ../../main_window/labels_view.rst:20
msgid "The digiKam Labels View from Left Sidebar"
msgstr "Pogled oznak digiKam z leve stranske vrstice"

#: ../../main_window/labels_view.rst:22
msgid ""
"The Labels View allows you to select photographs by the **Rating**, **Pick** "
"and **Color** Labels you assigned to them previously either by using:"
msgstr ""
"Pogled oznak vam omogoča, da izberete fotografije po oznakah **Ocena**, "
"**Izberi** in **Barva**, ki ste jim jih predhodno dodelili bodisi z uporabo:"

#: ../../main_window/labels_view.rst:24
msgid "The context menu of a thumbnail."
msgstr "Kontekstni meni sličice."

#: ../../main_window/labels_view.rst:30
msgid "The digiKam Icon-View Context Menu and Labels Options"
msgstr "Kontekstni meni pogleda ikon digiKam in možnosti oznak"

#: ../../main_window/labels_view.rst:32
msgid ""
"The **Description** tab of the **Captions** section on the Right Sidebar."
msgstr "Zavihek **Opis** razdelka **Napisi** na desni stranski vrstici."

#: ../../main_window/labels_view.rst:38
msgid "The digiKam Labels Options from Captions Right Sidebar Tab"
msgstr "Možnosti oznak digiKam na zavihku Napisi v desni stranski vrstici"

#: ../../main_window/labels_view.rst:40
msgid ""
"You may select more than one label by :kbd:`Ctrl+left` click. The selected "
"labels are connected by boolean AND, e.g. selecting Four Star and Yellow "
"will display only photographs that have both labels assigned. You can "
"perform even more sophisticated searches by using the **Filters** section of "
"the :ref:`Right Sidebar <filters_view>`."
msgstr ""
"Izberete lahko več kot eno oznako z :kbd:`Ctrl levim` klikom. \n"
"\n"
"Izbrane oznake so povezane z logičnim IN, npr. če izberete Štiri zvezdice in "
"Rumeno, bodo prikazane samo fotografije, ki imajo obe dodeljeni oznaki. "
"Izvedete lahko še bolj dodelana iskanja z uporabo razdelka **Filtri** v :ref:"
"`Right Sidebar <filters_view>`."

#: ../../main_window/labels_view.rst:46
msgid "The digiKam Labels Filter from Right Sidebar"
msgstr "Filter oznak digiKam iz desne stranske vrstice"

#: ../../main_window/labels_view.rst:50
msgid ""
"The **Pick Labels** properties can be assigned automatically using the deep-"
"learning tool **Image Quality Sorter** by analysis the aesthetic score of "
"items. See this :ref:`Maintenance Tool section <maintenance_quality>` for "
"details."
msgstr ""
"Lastnosti **Poberi značke** je mogoče samodejno dodeliti z uporabo orodja za "
"poglobljeno učenje **Image Quality Sorter** z analizo estetske ocene "
"elementov. Poglejte tole :ref:`Maintenance Tool section "
"<maintenance_quality>` za podrobnosti."

#: ../../main_window/labels_view.rst:52
msgid ""
"The **Rating** and **Color** Labels can be used to sort items from your "
"collection during your asset management workflow. See :ref:`this section "
"<rating_ranking>` for details."
msgstr ""
"Oznaki **Ocena** in **Barva** lahko uporabite za razvrščanje predmetov iz "
"vaše zbirke med potekom dela upravljanja sredstev. Poglejte :ref:`this "
"section <rating_ranking>` za podrobnosti."
