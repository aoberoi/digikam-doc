# Translation of docs_digikam_org_setup_application___shortcuts_settings.po to Catalan
# Copyright (C) 2023 This_file_is_part_of_KDE
# Licensed under the <a href="https://spdx.org/licenses/GFDL-1.2-or-later.html">terms of the GNU Free Documentation License 1.2</a> unless stated otherwise
# This file is distributed under the same license as the digiKam Manual package.
#
# Antoni Bella Pérez <antonibella5@yahoo.com>, 2023.
# Josep M. Ferrer <txemaq@gmail.com>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: digikam-doc\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-07-09 00:48+0000\n"
"PO-Revision-Date: 2023-05-02 20:29+0200\n"
"Last-Translator: Josep M. Ferrer <txemaq@gmail.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 20.12.0\n"

#: ../../setup_application/shortcuts_settings.rst:1
msgid "digiKam Shortcuts Settings"
msgstr "Configuració de les dreceres en el digiKam"

#: ../../setup_application/shortcuts_settings.rst:1
msgid ""
"digiKam, documentation, user manual, photo management, open source, free, "
"learn, easy, keyboard, shortcuts, setup, configure"
msgstr ""
"digiKam, documentació, manual d'usuari, gestió de les fotografies, codi "
"obert, lliure, aprenentatge, fàcil, teclat, dreceres, establir, configuració"

# skip-rule: common-settings
#: ../../setup_application/shortcuts_settings.rst:14
msgid "Shortcuts Settings"
msgstr "Configurar les dreceres"

#: ../../setup_application/shortcuts_settings.rst:16
msgid "Contents"
msgstr "Contingut"

#: ../../setup_application/shortcuts_settings.rst:19
msgid "Shortcuts Editor"
msgstr "Editor de dreceres"

#: ../../setup_application/shortcuts_settings.rst:21
msgid ""
"digiKam provide keyboard shortcuts that allow you to perform many tasks "
"without touching your mouse. If you use your keyboard frequently, using "
"these can save you lots of time."
msgstr ""
"El digiKam proporciona dreceres de teclat que permeten realitzar moltes "
"tasques sense tocar el ratolí. Si utilitzeu el teclat amb freqüència, "
"utilitzar-lo us pot estalviar molt de temps."

#: ../../setup_application/shortcuts_settings.rst:23
msgid ""
"digiKam has a dedicated keyboard shortcuts configuration dialog accessed via "
"the :menuselection:`Settings --> Configure Shortcuts` main menu item."
msgstr ""
"El digiKam ha dedicat un diàleg de configuració de dreceres de teclat, al "
"qual s'accedeix mitjançant l'element de menú «:menuselection:`Arranjament --"
"> Configura les dreceres de teclat...`»."

#: ../../setup_application/shortcuts_settings.rst:29
msgid "The digiKam Dialog To Configure The Keyboard Shortcuts"
msgstr "El diàleg del digiKam per a configurar les dreceres de teclat"

#: ../../setup_application/shortcuts_settings.rst:31
msgid ""
"In this dialog, you will see a list of all the shortcuts available in the "
"application. You can use the **Search** box at the top to search for the "
"shortcut you want."
msgstr ""
"En aquest diàleg, veureu una llista de totes les dreceres disponibles a "
"l'aplicació. Podreu utilitzar el quadre de **Cerca** a la part superior per "
"a cercar la drecera que vulgueu."

#: ../../setup_application/shortcuts_settings.rst:33
msgid ""
"To change a shortcut, first click on the name of a shortcut you want to "
"change. You will see a radio group where you can choose whether to set the "
"shortcut to its default value, or select a new shortcut for the selected "
"action. To set a new shortcut, choose **Custom** and click on the button "
"next to it. Then just type the shortcut you would like to use, and your "
"changes will be saved."
msgstr ""
"Per a canviar una drecera, primer feu clic sobre el nom de la drecera que "
"voleu canviar. Veureu un grup de botons d'opció on podreu escollir si "
"establiu la drecera al seu valor predeterminat o seleccioneu una drecera "
"nova per a l'acció seleccionada. Per a establir una drecera nova, trieu "
"**Personalitzada** i feu clic en el botó que hi ha al costat. Després, "
"senzillament escriviu la drecera que us agradaria utilitzar i es desaran els "
"vostres canvis."

#: ../../setup_application/shortcuts_settings.rst:35
msgid ""
"To reset a shortcut, there is a button at the bottom of the dialog, called "
"**Defaults**. Clicking on this button will reset all your custom shortcuts "
"to their default values. You can also reset an individual shortcut to its "
"default value by selecting it, and choosing the **Default** radio button."
msgstr ""
"Per a restablir una drecera, hi ha un botó a la part inferior del diàleg, "
"anomenat **Predeterminada**. Quan es faci clic en aquest botó, es "
"restabliran totes les dreceres personalitzades als valors predeterminats. "
"També podreu restablir una drecera individual al valor predeterminat "
"seleccionant-la i escollint el botó d'opció **Predeterminada**."

#: ../../setup_application/shortcuts_settings.rst:37
msgid ""
"To remove a shortcut, select it from the list, then click the black arrow "
"with a cross icon to the right of the button that allows you to select a "
"shortcut."
msgstr ""
"Per a eliminar una drecera, seleccioneu-la de la llista, després feu clic en "
"la fletxa negra amb una icona de creu a la dreta del botó que permet "
"seleccionar una drecera."

#: ../../setup_application/shortcuts_settings.rst:39
msgid ""
"To print-out a list of shortcuts for easy reference by clicking the "
"**Print** button at the bottom of the dialog."
msgstr ""
"Per a imprimir una llista de dreceres per a una referència fàcil, feu clic "
"sobre el botó **Imprimeix** a la part inferior del diàleg."

#: ../../setup_application/shortcuts_settings.rst:41
msgid ""
"You can also work with **Schemes**. These ones are keyboard shortcuts "
"configuration profiles, so you can create several profiles with different "
"shortcuts and switch between these profiles easily. To see a menu allowing "
"you to edit schemes, click on the **Manage Schemes** button at the bottom of "
"the dialog. The following options will appear:"
msgstr ""
"També podreu treballar amb els **Esquemes**. Aquests són perfils de "
"configuració de dreceres de teclat, de manera que podreu crear diversos "
"perfils amb dreceres diferents i canviar entre aquests perfils amb "
"facilitat. Per a veure un menú que us permeti editar els esquemes, feu clic "
"sobre el botó **Gestiona els esquemes** a la part inferior del diàleg. "
"Apareixeran les opcions següents:"

#: ../../setup_application/shortcuts_settings.rst:43
msgid "**Current Scheme**: Allows you to switch between your schemes."
msgstr "**Esquema actual**: permet canviar entre els vostres esquemes."

#: ../../setup_application/shortcuts_settings.rst:45
msgid ""
"**New**: Creates a new scheme. This opens a window that lets you select a "
"name for your new scheme."
msgstr ""
"**Nou**: Crea un esquema nou. Això obrirà una finestra que permeti "
"seleccionar un nom per al vostre esquema nou."

#: ../../setup_application/shortcuts_settings.rst:47
msgid "**Delete**: Deletes the current scheme."
msgstr "**Suprimeix**: suprimeix l'esquema actual."

#: ../../setup_application/shortcuts_settings.rst:49
msgid "**More Actions**: Opens the following menu:"
msgstr "**Més accions**: obre el menú següent:"

#: ../../setup_application/shortcuts_settings.rst:51
msgid ""
"**Save Shortcuts to scheme**: Save the current shortcuts to the current "
"scheme."
msgstr ""
"**Desa les dreceres a l'esquema**: desa les dreceres actuals a l'esquema "
"actual."

#: ../../setup_application/shortcuts_settings.rst:52
msgid "**Export Scheme**: Exports the current scheme to a file."
msgstr "**Exporta l'esquema**: exporta l'esquema actual cap a un fitxer."

#: ../../setup_application/shortcuts_settings.rst:53
msgid "**Import Scheme**: Imports a scheme from a file."
msgstr "**Importa un esquema**: importa un esquema des d'un fitxer."

#: ../../setup_application/shortcuts_settings.rst:56
msgid "Default Shortcuts"
msgstr "Dreceres predeterminades"

#: ../../setup_application/shortcuts_settings.rst:58
msgid "digiKam define these keyboard shortcuts by default."
msgstr ""
"El digiKam defineix aquestes dreceres de teclat de manera predeterminada."

#: ../../setup_application/shortcuts_settings.rst:61
msgid "Rating Assignment"
msgstr "Assignació de la valoració"

#: ../../setup_application/shortcuts_settings.rst:64
msgid "Rating"
msgstr "Valoració"

#: ../../setup_application/shortcuts_settings.rst:64
#: ../../setup_application/shortcuts_settings.rst:78
#: ../../setup_application/shortcuts_settings.rst:90
#: ../../setup_application/shortcuts_settings.rst:108
#: ../../setup_application/shortcuts_settings.rst:121
#: ../../setup_application/shortcuts_settings.rst:133
#: ../../setup_application/shortcuts_settings.rst:157
#: ../../setup_application/shortcuts_settings.rst:173
#: ../../setup_application/shortcuts_settings.rst:191
#: ../../setup_application/shortcuts_settings.rst:208
#: ../../setup_application/shortcuts_settings.rst:224
#: ../../setup_application/shortcuts_settings.rst:244
#: ../../setup_application/shortcuts_settings.rst:264
#: ../../setup_application/shortcuts_settings.rst:276
msgid "Shortcut"
msgstr "Drecera"

#: ../../setup_application/shortcuts_settings.rst:66
msgid "No rating"
msgstr "Sense valoració"

#: ../../setup_application/shortcuts_settings.rst:66
msgid ":kbd:`Ctrl+0`"
msgstr ":kbd:`Ctrl+0`"

#: ../../setup_application/shortcuts_settings.rst:67
msgid "1 star"
msgstr "1 estrella"

#: ../../setup_application/shortcuts_settings.rst:67
msgid ":kbd:`Ctrl+1`"
msgstr ":kbd:`Ctrl+1`"

#: ../../setup_application/shortcuts_settings.rst:68
msgid "2 stars"
msgstr "2 estrelles"

#: ../../setup_application/shortcuts_settings.rst:68
msgid ":kbd:`Ctrl+2`"
msgstr ":kbd:`Ctrl+2`"

#: ../../setup_application/shortcuts_settings.rst:69
msgid "3 stars"
msgstr "3 estrelles"

#: ../../setup_application/shortcuts_settings.rst:69
msgid ":kbd:`Ctrl+3`"
msgstr ":kbd:`Ctrl+3`"

#: ../../setup_application/shortcuts_settings.rst:70
msgid "4 stars"
msgstr "4 estrelles"

#: ../../setup_application/shortcuts_settings.rst:70
msgid ":kbd:`Ctrl+4`"
msgstr ":kbd:`Ctrl+4`"

#: ../../setup_application/shortcuts_settings.rst:71
msgid "5 stars"
msgstr "5 estrelles"

#: ../../setup_application/shortcuts_settings.rst:71
msgid ":kbd:`Ctrl+5`"
msgstr ":kbd:`Ctrl+5`"

#: ../../setup_application/shortcuts_settings.rst:75
msgid "Pick Label Assignment"
msgstr "Assignació dels rètols de selecció"

#: ../../setup_application/shortcuts_settings.rst:78
msgid "Pick Label"
msgstr "Rètol de selecció"

#: ../../setup_application/shortcuts_settings.rst:80
#: ../../setup_application/shortcuts_settings.rst:92
msgid "None"
msgstr "Cap"

#: ../../setup_application/shortcuts_settings.rst:80
msgid ":kbd:`Alt+0`"
msgstr ":kbd:`Alt+0`"

#: ../../setup_application/shortcuts_settings.rst:81
msgid "Rejected"
msgstr "Rebutjada"

#: ../../setup_application/shortcuts_settings.rst:81
msgid ":kbd:`Alt+1`"
msgstr ":kbd:`Alt+1`"

#: ../../setup_application/shortcuts_settings.rst:82
msgid "Pending"
msgstr "Pendent"

#: ../../setup_application/shortcuts_settings.rst:82
msgid ":kbd:`Alt+2`"
msgstr ":kbd:`Alt+2`"

#: ../../setup_application/shortcuts_settings.rst:83
msgid "Accepted"
msgstr "Acceptada"

#: ../../setup_application/shortcuts_settings.rst:83
msgid ":kbd:`Alt+3`"
msgstr ":kbd:`Alt+3`"

#: ../../setup_application/shortcuts_settings.rst:87
msgid "Color Label Assignment"
msgstr "Assignació dels rètols de color"

#: ../../setup_application/shortcuts_settings.rst:90
msgid "Color Label"
msgstr "Rètol de color"

#: ../../setup_application/shortcuts_settings.rst:92
msgid ":kbd:`Ctrl+Alt+0`"
msgstr ":kbd:`Ctrl+Alt+0`"

#: ../../setup_application/shortcuts_settings.rst:93
msgid "Red"
msgstr "Vermell"

#: ../../setup_application/shortcuts_settings.rst:93
msgid ":kbd:`Ctrl+Alt+1`"
msgstr ":kbd:`Ctrl+Alt+1`"

#: ../../setup_application/shortcuts_settings.rst:94
msgid "Orange"
msgstr "Taronja"

#: ../../setup_application/shortcuts_settings.rst:94
msgid ":kbd:`Ctrl+Alt+2`"
msgstr ":kbd:`Ctrl+Alt+2`"

#: ../../setup_application/shortcuts_settings.rst:95
msgid "Yellow"
msgstr "Groc"

#: ../../setup_application/shortcuts_settings.rst:95
msgid ":kbd:`Ctrl+Alt+3`"
msgstr ":kbd:`Ctrl+Alt+3`"

#: ../../setup_application/shortcuts_settings.rst:96
msgid "Green"
msgstr "Verd"

#: ../../setup_application/shortcuts_settings.rst:96
msgid ":kbd:`Ctrl+Alt+4`"
msgstr ":kbd:`Ctrl+Alt+4`"

#: ../../setup_application/shortcuts_settings.rst:97
msgid "Blue"
msgstr "Blau"

#: ../../setup_application/shortcuts_settings.rst:97
msgid ":kbd:`Ctrl+Alt+5`"
msgstr ":kbd:`Ctrl+Alt+5`"

#: ../../setup_application/shortcuts_settings.rst:98
msgid "Magenta"
msgstr "Magenta"

#: ../../setup_application/shortcuts_settings.rst:98
msgid ":kbd:`Ctrl+Alt+6`"
msgstr ":kbd:`Ctrl+Alt+6`"

#: ../../setup_application/shortcuts_settings.rst:99
msgid "Gray"
msgstr "Gris"

#: ../../setup_application/shortcuts_settings.rst:99
msgid ":kbd:`Ctrl+Alt+7`"
msgstr ":kbd:`Ctrl+Alt+7`"

#: ../../setup_application/shortcuts_settings.rst:100
msgid "Black"
msgstr "Negre"

#: ../../setup_application/shortcuts_settings.rst:100
msgid ":kbd:`Ctrl+Alt+8`"
msgstr ":kbd:`Ctrl+Alt+8`"

#: ../../setup_application/shortcuts_settings.rst:101
msgid "White"
msgstr "Blanc"

#: ../../setup_application/shortcuts_settings.rst:101
msgid ":kbd:`Ctrl+Alt+9`"
msgstr ":kbd:`Ctrl+Alt+9`"

#: ../../setup_application/shortcuts_settings.rst:105
msgid "Zooming"
msgstr "Fer «zoom»"

#: ../../setup_application/shortcuts_settings.rst:108
#: ../../setup_application/shortcuts_settings.rst:121
#: ../../setup_application/shortcuts_settings.rst:133
#: ../../setup_application/shortcuts_settings.rst:157
#: ../../setup_application/shortcuts_settings.rst:173
#: ../../setup_application/shortcuts_settings.rst:191
#: ../../setup_application/shortcuts_settings.rst:208
#: ../../setup_application/shortcuts_settings.rst:224
#: ../../setup_application/shortcuts_settings.rst:244
#: ../../setup_application/shortcuts_settings.rst:264
#: ../../setup_application/shortcuts_settings.rst:276
msgid "Action"
msgstr "Acció"

#: ../../setup_application/shortcuts_settings.rst:110
msgid "Zoom in"
msgstr "Apropa"

#: ../../setup_application/shortcuts_settings.rst:110
msgid ":kbd:`Ctrl++`"
msgstr ":kbd:`Ctrl++`"

#: ../../setup_application/shortcuts_settings.rst:111
msgid "Zoom out"
msgstr "Allunya"

#: ../../setup_application/shortcuts_settings.rst:111
msgid ":kbd:`Ctrl+-`"
msgstr ":kbd:`Ctrl+-`"

#: ../../setup_application/shortcuts_settings.rst:112
msgid "Zoom 100%"
msgstr "Zoom al 100%"

#: ../../setup_application/shortcuts_settings.rst:112
msgid ":kbd:`Ctrl+.`"
msgstr ":kbd:`Ctrl+.`"

#: ../../setup_application/shortcuts_settings.rst:113
msgid "Fit to window"
msgstr "Ajusta a la finestra"

#: ../../setup_application/shortcuts_settings.rst:113
msgid ":kbd:`Ctrl+Alt+E`"
msgstr ":kbd:`Ctrl+Alt+E`"

#: ../../setup_application/shortcuts_settings.rst:114
msgid "Fit to selection"
msgstr "Ajusta a la selecció"

#: ../../setup_application/shortcuts_settings.rst:114
msgid ":kbd:`Ctrl+Alt+S`"
msgstr ":kbd:`Ctrl+Alt+S`"

#: ../../setup_application/shortcuts_settings.rst:118
msgid "Slide-Show"
msgstr "Presentació de diapositives"

#: ../../setup_application/shortcuts_settings.rst:123
msgid "Play All"
msgstr "Reprodueix-ho tot"

#: ../../setup_application/shortcuts_settings.rst:123
msgid ":kbd:`F9`"
msgstr ":kbd:`F9`"

#: ../../setup_application/shortcuts_settings.rst:124
msgid "Play Selection"
msgstr "Reprodueix la selecció"

#: ../../setup_application/shortcuts_settings.rst:124
msgid ":kbd:`Alt+F9`"
msgstr ":kbd:`Alt+F9`"

#: ../../setup_application/shortcuts_settings.rst:125
msgid "Play with Sub-Albums"
msgstr "Reprodueix amb els subàlbums"

#: ../../setup_application/shortcuts_settings.rst:125
msgid ":kbd:`Shift+F9`"
msgstr ":kbd:`Maj+F9`"

#: ../../setup_application/shortcuts_settings.rst:126
msgid "Presentation"
msgstr "Presentació"

#: ../../setup_application/shortcuts_settings.rst:126
msgid ":kbd:`Shift+Alt+F9`"
msgstr ":kbd:`Maj+Alt+F9`"

#: ../../setup_application/shortcuts_settings.rst:130
msgid "Views Control"
msgstr "Control de les vistes"

#: ../../setup_application/shortcuts_settings.rst:135
msgid "Albums View"
msgstr "La vista d'àlbums"

#: ../../setup_application/shortcuts_settings.rst:135
msgid ":kbd:`Shift+Ctrl+F1`"
msgstr ":kbd:`Maj+Ctrl+F1`"

#: ../../setup_application/shortcuts_settings.rst:136
msgid "Tags View"
msgstr "La vista d'etiquetes"

#: ../../setup_application/shortcuts_settings.rst:136
msgid ":kbd:`Shift+Ctrl+F2`"
msgstr ":kbd:`Maj+Ctrl+F2`"

#: ../../setup_application/shortcuts_settings.rst:137
msgid "Labels View"
msgstr "La vista de rètols"

#: ../../setup_application/shortcuts_settings.rst:137
msgid ":kbd:`Shift+Ctrl+F3`"
msgstr ":kbd:`Maj+Ctrl+F3`"

#: ../../setup_application/shortcuts_settings.rst:138
msgid "Dates view"
msgstr "La vista de dates"

#: ../../setup_application/shortcuts_settings.rst:138
msgid ":kbd:`Shift+Ctrl+F4`"
msgstr ":kbd:`Maj+Ctrl+F4`"

#: ../../setup_application/shortcuts_settings.rst:139
msgid "Timeline View"
msgstr "La vista de la línia de temps"

#: ../../setup_application/shortcuts_settings.rst:139
msgid ":kbd:`Shift+Ctrl+F5`"
msgstr ":kbd:`Maj+Ctrl+F5`"

#: ../../setup_application/shortcuts_settings.rst:140
msgid "Search View"
msgstr "La vista de cerca"

#: ../../setup_application/shortcuts_settings.rst:140
msgid ":kbd:`Shift+Ctrl+F6`"
msgstr ":kbd:`Maj+Ctrl+F6`"

#: ../../setup_application/shortcuts_settings.rst:141
msgid "Similarity View"
msgstr "Vista de similitud"

#: ../../setup_application/shortcuts_settings.rst:141
msgid ":kbd:`Shift+Ctrl+F7`"
msgstr ":kbd:`Maj+Ctrl+F7`"

#: ../../setup_application/shortcuts_settings.rst:142
msgid "Map View"
msgstr "La vista de mapa"

#: ../../setup_application/shortcuts_settings.rst:142
msgid ":kbd:`Shift+Ctrl+F8`"
msgstr ":kbd:`Maj+Ctrl+F8`"

#: ../../setup_application/shortcuts_settings.rst:143
msgid "People View"
msgstr "La vista de persones"

#: ../../setup_application/shortcuts_settings.rst:143
msgid ":kbd:`Shift+Ctrl+F9`"
msgstr ":kbd:`Maj+Ctrl+F9`"

#: ../../setup_application/shortcuts_settings.rst:144
msgid "Full Screen Mode"
msgstr "Mode de pantalla completa"

#: ../../setup_application/shortcuts_settings.rst:144
msgid ":kbd:`Ctrl+Shift+F`"
msgstr ":kbd:`Ctrl+Maj+F`"

#: ../../setup_application/shortcuts_settings.rst:145
msgid "Preview"
msgstr "Vista prèvia"

#: ../../setup_application/shortcuts_settings.rst:145
msgid ":kbd:`F3`"
msgstr ":kbd:`F3`"

#: ../../setup_application/shortcuts_settings.rst:146
msgid "Exit Preview Mode"
msgstr "Sortida del mode de vista prèvia"

#: ../../setup_application/shortcuts_settings.rst:146
msgid ":kbd:`Esc`"
msgstr ":kbd:`Esc`"

#: ../../setup_application/shortcuts_settings.rst:147
msgid "Toggle Left Side-bar"
msgstr "Commuta la barra lateral esquerra"

#: ../../setup_application/shortcuts_settings.rst:147
msgid ":kbd:`Ctrl+Alt+Left`"
msgstr ":kbd:`Ctrl+Alt+Fletxa esquerra`"

#: ../../setup_application/shortcuts_settings.rst:148
msgid "Toggle Right Side-bar"
msgstr "Commuta la barra lateral dreta"

#: ../../setup_application/shortcuts_settings.rst:148
msgid ":kbd:`Ctrl+Alt+Right`"
msgstr ":kbd:`Ctrl+Alt+Fletxa dreta`"

#: ../../setup_application/shortcuts_settings.rst:149
msgid "Refresh"
msgstr "Actualitza"

#: ../../setup_application/shortcuts_settings.rst:149
msgid ":kbd:`F5`"
msgstr ":kbd:`F5`"

#: ../../setup_application/shortcuts_settings.rst:150
msgid "Turn On/Off Color Management View"
msgstr "Activa/desactiva la gestió del color"

#: ../../setup_application/shortcuts_settings.rst:150
msgid ":kbd:`F12`"
msgstr ":kbd:`F12`"

#: ../../setup_application/shortcuts_settings.rst:154
msgid "Main Tools"
msgstr "Eines principals"

#: ../../setup_application/shortcuts_settings.rst:159
msgid "Open in Editor"
msgstr "Obre a l'editor"

#: ../../setup_application/shortcuts_settings.rst:159
msgid ":kbd:`F4`"
msgstr ":kbd:`F4`"

#: ../../setup_application/shortcuts_settings.rst:160
msgid "Open in Default Application"
msgstr "Obre a l'aplicació predeterminada"

#: ../../setup_application/shortcuts_settings.rst:160
msgid ":kbd:`Ctrl+F4`"
msgstr ":kbd:`Ctrl+F4`"

#: ../../setup_application/shortcuts_settings.rst:161
msgid "Light Table"
msgstr "Taula de llum"

#: ../../setup_application/shortcuts_settings.rst:161
msgid ":kbd:`Shift+L`"
msgstr ":kbd:`Maj+L`"

#: ../../setup_application/shortcuts_settings.rst:162
msgid "Place on Light Table"
msgstr "Obre a la Taula de llum"

#: ../../setup_application/shortcuts_settings.rst:162
msgid ":kbd:`Ctrl+L`"
msgstr ":kbd:`Ctrl+L`"

#: ../../setup_application/shortcuts_settings.rst:163
msgid "Add to Light Table"
msgstr "Afegeix a la Taula de llum"

#: ../../setup_application/shortcuts_settings.rst:163
msgid ":kbd:`Ctrl+Shift+L`"
msgstr ":kbd:`Ctrl+Maj+L`"

#: ../../setup_application/shortcuts_settings.rst:164
msgid "Batch Queue Manager"
msgstr "Gestor de la cua per lots"

#: ../../setup_application/shortcuts_settings.rst:164
msgid ":kbd:`Shift+B`"
msgstr ":kbd:`Maj+B`"

#: ../../setup_application/shortcuts_settings.rst:165
msgid "Add to Current Queue"
msgstr "Afegeix a la cua actual"

#: ../../setup_application/shortcuts_settings.rst:165
msgid ":kbd:`Ctrl+B`"
msgstr ":kbd:`Ctrl+B`"

#: ../../setup_application/shortcuts_settings.rst:166
msgid "Add to new Queue"
msgstr "Afegeix a una cua nova"

#: ../../setup_application/shortcuts_settings.rst:166
msgid ":kbd:`Ctrl+Shift+B`"
msgstr ":kbd:`Ctrl+Maj+B`"

#: ../../setup_application/shortcuts_settings.rst:170
msgid "Items Navigation"
msgstr "Navegació pels elements"

#: ../../setup_application/shortcuts_settings.rst:175
msgid "Back"
msgstr "Enrere"

#: ../../setup_application/shortcuts_settings.rst:175
msgid ":kbd:`Alt+Left`"
msgstr ":kbd:`Alt+Fletxa esquerra`"

#: ../../setup_application/shortcuts_settings.rst:176
msgid "Forward"
msgstr "Endavant"

#: ../../setup_application/shortcuts_settings.rst:176
msgid ":kbd:`Alt+Right`"
msgstr ":kbd:`Alt+Fletxa dreta`"

#: ../../setup_application/shortcuts_settings.rst:177
msgid "First Image"
msgstr "Primera imatge"

#: ../../setup_application/shortcuts_settings.rst:177
msgid ":kbd:`Ctrl+Home`"
msgstr ":kbd:`Ctrl+Inici`"

#: ../../setup_application/shortcuts_settings.rst:178
msgid "Last Image"
msgstr "Darrera imatge"

#: ../../setup_application/shortcuts_settings.rst:178
msgid ":kbd:`Ctrl+End`"
msgstr ":kbd:`Ctrl+Fi`"

#: ../../setup_application/shortcuts_settings.rst:179
msgid "Next Left Side-bar Tab"
msgstr "Pestanya següent de la barra lateral esquerra"

#: ../../setup_application/shortcuts_settings.rst:179
msgid ":kbd:`Ctrl+Alt+End`"
msgstr ":kbd:`Ctrl+Alt+Fi`"

#: ../../setup_application/shortcuts_settings.rst:180
msgid "Next Right Side-bar Tab"
msgstr "Pestanya següent de la barra lateral dreta"

#: ../../setup_application/shortcuts_settings.rst:180
msgid ":kbd:`Ctrl+Alt+PgDown`"
msgstr ":kbd:`Ctrl+Alt+Av Pàg`"

#: ../../setup_application/shortcuts_settings.rst:181
msgid "Next Image"
msgstr "Imatge següent"

#: ../../setup_application/shortcuts_settings.rst:181
msgid ":kbd:`Space`"
msgstr ":kbd:`Espai`"

#: ../../setup_application/shortcuts_settings.rst:182
msgid "Previous Image"
msgstr "Imatge anterior"

#: ../../setup_application/shortcuts_settings.rst:182
msgid ":kbd:`Backspace`"
msgstr ":kbd:`Retrocés`"

#: ../../setup_application/shortcuts_settings.rst:183
msgid "Previous Left Side-bar Tab"
msgstr "Pestanya anterior de la barra lateral esquerra"

#: ../../setup_application/shortcuts_settings.rst:183
msgid ":kbd:`Ctrl+Alt+Home`"
msgstr ":kbd:`Ctrl+Alt+Inici`"

#: ../../setup_application/shortcuts_settings.rst:184
msgid "Previous Right Side-bar Tab"
msgstr "Pestanya anterior de la barra lateral dreta"

#: ../../setup_application/shortcuts_settings.rst:184
msgid ":kbd:`Ctrl+Alt+PgUp`"
msgstr ":kbd:`Ctrl+Alt+Re Pàg`"

#: ../../setup_application/shortcuts_settings.rst:188
msgid "Item Manipulation"
msgstr "Manipulació dels elements"

#: ../../setup_application/shortcuts_settings.rst:193
msgid "Rename Item"
msgstr "Reanomena l'element"

#: ../../setup_application/shortcuts_settings.rst:193
msgid ":kbd:`F2`"
msgstr ":kbd:`F2`"

#: ../../setup_application/shortcuts_settings.rst:194
msgid "Rename Album"
msgstr "Reanomena l'àlbum"

#: ../../setup_application/shortcuts_settings.rst:194
msgid ":kbd:`Shift+F2`"
msgstr ":kbd:`Maj+F2`"

#: ../../setup_application/shortcuts_settings.rst:195
msgid "New Album"
msgstr "Àlbum nou"

#: ../../setup_application/shortcuts_settings.rst:195
msgid ":kbd:`Ctrl+N`"
msgstr ":kbd:`Ctrl+N`"

#: ../../setup_application/shortcuts_settings.rst:196
msgid "Move Item to Trash"
msgstr "Mou els elements a la paperera"

#: ../../setup_application/shortcuts_settings.rst:196
msgid ":kbd:`Del`"
msgstr ":kbd:`Supr`"

#: ../../setup_application/shortcuts_settings.rst:197
msgid "Delete Item permanently"
msgstr "Suprimeix definitivament els elements"

#: ../../setup_application/shortcuts_settings.rst:197
msgid ":kbd:`Shift+Del`"
msgstr ":kbd:`Maj+Supr`"

#: ../../setup_application/shortcuts_settings.rst:198
msgid "Rotate Item Left"
msgstr "Gira l'element a l'esquerra"

#: ../../setup_application/shortcuts_settings.rst:198
msgid ":kbd:`Ctrl+Shift+Left`"
msgstr ":kbd:`Ctrl+Maj+Fletxa esquerra`"

#: ../../setup_application/shortcuts_settings.rst:199
msgid "Rotate Item Right"
msgstr "Gira l'element a la dreta"

#: ../../setup_application/shortcuts_settings.rst:199
msgid ":kbd:`Ctrl+Shift+Right`"
msgstr ":kbd:`Ctrl+Maj+Fletxa dreta`"

#: ../../setup_application/shortcuts_settings.rst:200
msgid "Flip Item Horizontally"
msgstr "Inverteix horitzontalment l'element"

#: ../../setup_application/shortcuts_settings.rst:200
msgid ":kbd:`Ctrl+*`"
msgstr ":kbd:`Ctrl+*`"

#: ../../setup_application/shortcuts_settings.rst:201
msgid "Flip Item Vertically"
msgstr "Inverteix verticalment l'element"

#: ../../setup_application/shortcuts_settings.rst:201
msgid ":kbd:`Ctrl+/`"
msgstr ":kbd:`Ctrl+/`"

#: ../../setup_application/shortcuts_settings.rst:205
msgid "Application Operations"
msgstr "Operacions de l'aplicació"

#: ../../setup_application/shortcuts_settings.rst:210
msgid "Close Window"
msgstr "Tanca la finestra"

#: ../../setup_application/shortcuts_settings.rst:210
msgid ":kbd:`Alt+F4`"
msgstr ":kbd:`Alt+F4`"

#: ../../setup_application/shortcuts_settings.rst:211
msgid "Quit Application"
msgstr "Surt de l'aplicació"

#: ../../setup_application/shortcuts_settings.rst:211
msgid ":kbd:`Ctrl+Q`"
msgstr ":kbd:`Ctrl+Q`"

#: ../../setup_application/shortcuts_settings.rst:212
msgid "Configure Application"
msgstr "Configura l'aplicació"

#: ../../setup_application/shortcuts_settings.rst:212
msgid ":kbd:`Ctrl+Shift+,`"
msgstr ":kbd:`Ctrl+Maj+,`"

#: ../../setup_application/shortcuts_settings.rst:213
msgid "Configure Keyboard Shortcuts"
msgstr "Configura les dreceres de teclat"

#: ../../setup_application/shortcuts_settings.rst:213
msgid ":kbd:`Ctrl+Alt`"
msgstr ":kbd:`Ctrl+Alt`"

#: ../../setup_application/shortcuts_settings.rst:214
msgid "Show Menubar"
msgstr "Mostra la barra de menús"

#: ../../setup_application/shortcuts_settings.rst:214
msgid ":kbd:`Ctrl+M`"
msgstr ":kbd:`Ctrl+M`"

#: ../../setup_application/shortcuts_settings.rst:215
msgid "Show Thumbbar"
msgstr "Mostra la barra de miniatures"

#: ../../setup_application/shortcuts_settings.rst:215
msgid ":kbd:`Ctrl+T`"
msgstr ":kbd:`Ctrl+T`"

#: ../../setup_application/shortcuts_settings.rst:216
msgid "Find Action in Menu"
msgstr "Troba una acció en el menú"

#: ../../setup_application/shortcuts_settings.rst:216
msgid ":kbd:`Ctrl+Alt+I`"
msgstr ":kbd:`Ctrl+Alt+I`"

#: ../../setup_application/shortcuts_settings.rst:217
msgid "What's This?"
msgstr "Què és això?"

#: ../../setup_application/shortcuts_settings.rst:217
msgid ":kbd:`Shift+F1`"
msgstr ":kbd:`Maj+F1`"

#: ../../setup_application/shortcuts_settings.rst:221
msgid "Post Processing"
msgstr "Postprocessament"

#: ../../setup_application/shortcuts_settings.rst:226
msgid "Edit Album Properties"
msgstr "Edita les propietats de l'àlbum"

#: ../../setup_application/shortcuts_settings.rst:226
msgid ":kbd:`Alt+Return`"
msgstr ":kbd:`Alt+Retorn`"

#: ../../setup_application/shortcuts_settings.rst:227
msgid "Edit Comments"
msgstr "Edita els comentaris"

#: ../../setup_application/shortcuts_settings.rst:227
msgid ":kbd:`Alt+Shift+C`"
msgstr ":kbd:`Alt+Maj+C`"

#: ../../setup_application/shortcuts_settings.rst:228
msgid "Edit Titles"
msgstr "Edita els títols"

#: ../../setup_application/shortcuts_settings.rst:228
msgid ":kbd:`Alt+Shift+T`"
msgstr ":kbd:`Alt+Maj+T`"

#: ../../setup_application/shortcuts_settings.rst:229
msgid "Edit Geolocation"
msgstr "Edita la geolocalització"

#: ../../setup_application/shortcuts_settings.rst:229
msgid ":kbd:`Ctrl+Shift+G`"
msgstr ":kbd:`Ctrl+Maj+G`"

#: ../../setup_application/shortcuts_settings.rst:230
msgid "Edit Metadata"
msgstr "Edita les metadades"

#: ../../setup_application/shortcuts_settings.rst:230
msgid ":kbd:`Ctrl+Shift+M`"
msgstr ":kbd:`Ctrl+Maj+M`"

#: ../../setup_application/shortcuts_settings.rst:231
msgid "Assign Tag"
msgstr "Assigna una etiqueta"

#: ../../setup_application/shortcuts_settings.rst:231
msgid ":kbd:`T`"
msgstr ":kbd:`T`"

#: ../../setup_application/shortcuts_settings.rst:232
msgid "Show Assigned Tags"
msgstr "Mostra les etiquetes assignades"

#: ../../setup_application/shortcuts_settings.rst:232
msgid ":kbd:`Alt+Shift+A`"
msgstr ":kbd:`Alt+Maj+A`"

#: ../../setup_application/shortcuts_settings.rst:233
msgid "Adjust Date and Time"
msgstr "Ajusta la data i l'hora"

#: ../../setup_application/shortcuts_settings.rst:233
msgid ":kbd:`Ctrl+Shift+D`"
msgstr ":kbd:`Ctrl+Maj+D`"

#: ../../setup_application/shortcuts_settings.rst:234
msgid "Create Html gallery"
msgstr "Crea una galeria HTML"

#: ../../setup_application/shortcuts_settings.rst:234
msgid ":kbd:`Ctrl+Alt+Shift+H`"
msgstr ":kbd:`Ctrl+Alt+Maj+H`"

#: ../../setup_application/shortcuts_settings.rst:235
msgid "Search Text"
msgstr "Cerca el text"

#: ../../setup_application/shortcuts_settings.rst:235
msgid ":kbd:`Ctrl+F`"
msgstr ":kbd:`Ctrl+F`"

#: ../../setup_application/shortcuts_settings.rst:236
msgid "Advanced Search"
msgstr "Cerca avançada"

#: ../../setup_application/shortcuts_settings.rst:236
msgid ":kbd:`Ctrl+Alt+F`"
msgstr ":kbd:`Ctrl+Alt+F`"

#: ../../setup_application/shortcuts_settings.rst:237
msgid "Find Duplicates"
msgstr "Troba els duplicats"

#: ../../setup_application/shortcuts_settings.rst:237
msgid ":kbd:`Ctrl+D`"
msgstr ":kbd:`Ctrl+D`"

#: ../../setup_application/shortcuts_settings.rst:241
msgid "Export Tools"
msgstr "Eines d'exportació"

#: ../../setup_application/shortcuts_settings.rst:246
msgid "Export to Box"
msgstr "Exporta a Box"

#: ../../setup_application/shortcuts_settings.rst:246
msgid ":kbd:`Ctrl+Alt+Shift+B`"
msgstr ":kbd:`Ctrl+Alt+Maj+B`"

#: ../../setup_application/shortcuts_settings.rst:247
msgid "Export to Dropbox"
msgstr "Exporta a Dropbox"

#: ../../setup_application/shortcuts_settings.rst:247
msgid ":kbd:`Ctrl+Alt+Shift+D`"
msgstr ":kbd:`Ctrl+Alt+Maj+D`"

#: ../../setup_application/shortcuts_settings.rst:248
msgid "Export to Flickr"
msgstr "Exporta a Flickr"

#: ../../setup_application/shortcuts_settings.rst:248
msgid ":kbd:`Ctrl+Alt+Shift+R`"
msgstr ":kbd:`Ctrl+Alt+Maj+R`"

#: ../../setup_application/shortcuts_settings.rst:249
msgid "Export to Google Drive"
msgstr "Exporta a Google Drive"

#: ../../setup_application/shortcuts_settings.rst:249
msgid ":kbd:`Ctrl+Alt+Shift+G`"
msgstr ":kbd:`Ctrl+Alt+Maj+G`"

#: ../../setup_application/shortcuts_settings.rst:250
msgid "Export to Google Photos"
msgstr "Exporta a Google Photos"

#: ../../setup_application/shortcuts_settings.rst:250
msgid ":kbd:`Ctrl+Alt+Shift+P`"
msgstr ":kbd:`Ctrl+Alt+Maj+P`"

#: ../../setup_application/shortcuts_settings.rst:251
msgid "Export to Imageshack"
msgstr "Exporta cap a ImageShack"

#: ../../setup_application/shortcuts_settings.rst:251
msgid ":kbd:`Ctrl+Alt+Shift+M`"
msgstr ":kbd:`Ctrl+Alt+Maj+M`"

#: ../../setup_application/shortcuts_settings.rst:252
msgid "Export to iNaturalist"
msgstr "Exporta a iNaturalist"

#: ../../setup_application/shortcuts_settings.rst:252
msgid ":kbd:`Ctrl+Alt+Shift+N`"
msgstr ":kbd:`Ctrl+Alt+Maj+N`"

#: ../../setup_application/shortcuts_settings.rst:253
msgid "Export to local storage"
msgstr "Exporta a un emmagatzematge local"

#: ../../setup_application/shortcuts_settings.rst:253
msgid ":kbd:`Ctrl+Alt+Shift+L`"
msgstr ":kbd:`Ctrl+Alt+Maj+L`"

#: ../../setup_application/shortcuts_settings.rst:254
msgid "Export to Onedrive"
msgstr "Exporta a Onedrive"

#: ../../setup_application/shortcuts_settings.rst:254
msgid ":kbd:`Ctrl+Alt+Shift+O`"
msgstr ":kbd:`Ctrl+Alt+Maj+O`"

#: ../../setup_application/shortcuts_settings.rst:255
msgid "Export to Pinterest"
msgstr "Exporta a Pinterest"

#: ../../setup_application/shortcuts_settings.rst:255
msgid ":kbd:`Ctrl+Alt+Shift+I`"
msgstr ":kbd:`Ctrl+Alt+Maj+I`"

#: ../../setup_application/shortcuts_settings.rst:256
msgid "Export to remote storage"
msgstr "Exporta a un emmagatzematge remot"

#: ../../setup_application/shortcuts_settings.rst:256
msgid ":kbd:`Ctrl+Alt+Shift+K`"
msgstr ":kbd:`Ctrl+Alt+Maj+K`"

#: ../../setup_application/shortcuts_settings.rst:257
msgid "Export to SmugMug"
msgstr "Exporta a SmugMug"

#: ../../setup_application/shortcuts_settings.rst:257
msgid ":kbd:`Ctrl+Alt+Shift+S`"
msgstr ":kbd:`Ctrl+Alt+Maj+S`"

#: ../../setup_application/shortcuts_settings.rst:261
msgid "Import Tools"
msgstr "Eines d'importació"

#: ../../setup_application/shortcuts_settings.rst:266
msgid "Add Images"
msgstr "Afegeix imatges"

#: ../../setup_application/shortcuts_settings.rst:266
msgid ":kbd:`Alt+Shift+I`"
msgstr ":kbd:`Alt+Maj+I`"

#: ../../setup_application/shortcuts_settings.rst:267
msgid "Import from Google Photos"
msgstr "Importa des de Google Photos"

#: ../../setup_application/shortcuts_settings.rst:267
msgid ":kbd:`Alt+Shift+P`"
msgstr ":kbd:`Alt+Maj+P`"

#: ../../setup_application/shortcuts_settings.rst:268
msgid "Import from remote storage"
msgstr "Importa des d'un emmagatzematge remot"

#: ../../setup_application/shortcuts_settings.rst:268
msgid ":kbd:`Alt+Shift+K`"
msgstr ":kbd:`Alt+Maj+K`"

#: ../../setup_application/shortcuts_settings.rst:269
msgid "Import from SmugMug"
msgstr "Importa des de SmugMug"

#: ../../setup_application/shortcuts_settings.rst:269
msgid ":kbd:`Alt+Shift+S`"
msgstr ":kbd:`Alt+Maj+S`"

#: ../../setup_application/shortcuts_settings.rst:273
msgid "Selection Operations"
msgstr "Operacions de selecció"

#: ../../setup_application/shortcuts_settings.rst:278
msgid "Copy"
msgstr "Copia"

#: ../../setup_application/shortcuts_settings.rst:278
msgid ":kbd:`Ctrl+C`"
msgstr ":kbd:`Ctrl+C`"

#: ../../setup_application/shortcuts_settings.rst:279
msgid "Cut"
msgstr "Retalla"

#: ../../setup_application/shortcuts_settings.rst:279
msgid ":kbd:`Ctrl+X`"
msgstr ":kbd:`Ctrl+X`"

#: ../../setup_application/shortcuts_settings.rst:280
msgid "Paste"
msgstr "Enganxa"

#: ../../setup_application/shortcuts_settings.rst:280
msgid ":kbd:`Ctrl+V`"
msgstr ":kbd:`Ctrl+V`"

#: ../../setup_application/shortcuts_settings.rst:281
msgid "Invert Selection"
msgstr "Inverteix la selecció"

#: ../../setup_application/shortcuts_settings.rst:281
msgid ":kbd:`Ctrl+I`"
msgstr ":kbd:`Ctrl+I`"

#: ../../setup_application/shortcuts_settings.rst:282
msgid "Select All"
msgstr "Selecciona-ho tot"

#: ../../setup_application/shortcuts_settings.rst:282
msgid ":kbd:`Ctrl+A`"
msgstr ":kbd:`Ctrl+A`"

#: ../../setup_application/shortcuts_settings.rst:283
msgid "Select None"
msgstr "No seleccionis res"

#: ../../setup_application/shortcuts_settings.rst:283
msgid ":kbd:`Ctrl+Shift+A`"
msgstr ":kbd:`Ctrl+Maj+A`"
