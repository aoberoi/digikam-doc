# Translation of docs_digikam_org_image_editor___workflow_tools.po to Catalan
# Copyright (C) 2023 This_file_is_part_of_KDE
# Licensed under the <a href="https://spdx.org/licenses/GFDL-1.2-or-later.html">terms of the GNU Free Documentation License 1.2</a> unless stated otherwise
# This file is distributed under the same license as the digiKam Manual package.
#
# Antoni Bella Pérez <antonibella5@yahoo.com>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: digikam-doc\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-07-09 00:48+0000\n"
"PO-Revision-Date: 2023-02-27 23:15+0100\n"
"Last-Translator: Antoni Bella Pérez <antonibella5@yahoo.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 22.12.2\n"

#: ../../image_editor/workflow_tools.rst:1
msgid "digiKam Image Editor Workflow Tools"
msgstr "Eines del flux de treball en l'editor d'imatges del digiKam"

#: ../../image_editor/workflow_tools.rst:1
msgid ""
"digiKam, documentation, user manual, photo management, open source, free, "
"learn, easy, image, editor, workflow, tools, color, enhance, transform, "
"effects, decorate"
msgstr ""
"digiKam, documentació, manual d'usuari, gestió de les fotografies, codi "
"obert, lliure, aprenentatge, fàcil, imatge, editor, flux de treball, eines, "
"color, millorar, transformar, efectes, decorar"

#: ../../image_editor/workflow_tools.rst:14
msgid "Workflow Tools"
msgstr "Eines del flux de treball"

#: ../../image_editor/workflow_tools.rst:16
msgid "Contents"
msgstr "Contingut"

#: ../../image_editor/workflow_tools.rst:21
msgid "A Standard Workflow Proposal"
msgstr "Una proposta de flux de treball estàndard"

#: ../../image_editor/workflow_tools.rst:23
msgid ""
"When performing a basic workflow, the primary areas for consideration are:"
msgstr ""
"Quan es realitza un flux de treball bàsic, les àrees principals a considerar "
"són:"

#: ../../image_editor/workflow_tools.rst:25
msgid "Exposure: :ref:`White Balance <color_wb>`."
msgstr "Exposició: :ref:`Balanç de blancs <color_wb>`."

#: ../../image_editor/workflow_tools.rst:27
msgid "Color: :ref:`White Balance <color_wb>`."
msgstr "Color: :ref:`Balanç de blancs <color_wb>`."

#: ../../image_editor/workflow_tools.rst:29
msgid ""
"Black and white points: :ref:`White Balance <color_wb>` or :ref:`Adjust "
"Levels <color_levels>`."
msgstr ""
"Punts negres i blancs: :ref:`Balanç de blancs <color_wb>` o :ref:`Ajust dels "
"nivells <color_levels>`."

#: ../../image_editor/workflow_tools.rst:31
msgid "Contrast: :ref:`Adjust Curves <color_levels>`."
msgstr "Contrast: :ref:`Ajust de les corbes <color_levels>`."

#: ../../image_editor/workflow_tools.rst:33
msgid ""
"Saturation: :ref:`White Balance <color_wb>`, :ref:`Vivid <effects_color>` "
"or :ref:`Hue/Saturation/Lightness <color_hsl>`."
msgstr ""
"Saturació: :ref:`Balanç de blancs <color_wb>`, :ref:`Llampant "
"<effects_color>` o :ref:`To/Saturació/Claredat <color_hsl>`."

#: ../../image_editor/workflow_tools.rst:35
msgid "Resizing (interpolation): :ref:`Change Size <transform_resize>`."
msgstr ""
"Canvi de mida (interpolació): :ref:`Canviar la mida <transform_resize>`."

#: ../../image_editor/workflow_tools.rst:37
msgid ":ref:`Sharpening <enhance_sharpen>`."
msgstr ":ref:`Aguditza <enhance_sharpen>`."

#: ../../image_editor/workflow_tools.rst:39
msgid ""
":ref:`Digital Asset Management <organize_find>` (applying tags, captions, "
"rating, geolocation, save under new name)."
msgstr ""
":ref:`Gestió d'actius digitals <organize_find>` (aplicar etiquetes, "
"llegendes, valoració, geolocalització, desar amb un nom nou)."

#: ../../image_editor/workflow_tools.rst:41
msgid ""
"We recommend this sequence of image editing steps to ensure optimum "
"photographic quality for the end product. In particular, never do sharpening "
"before the last step. And we recommend using a lossless format like PNG or "
"TIFF for storing photographs. Otherwise you'll lose a bit every time you "
"save the results. If you can afford it, use 16 bit/channel for your best "
"shots. If you want to change color space, in particular if you want to work "
"in CIEL*a*b, 16 bit per channel are required to avoid truncation effects."
msgstr ""
"Us recomanem aquesta seqüència de passos d'edició d'imatges per a garantir "
"una qualitat fotogràfica òptima per al producte final. En particular, no "
"aguditzeu mai abans de l'últim pas. I recomanem utilitzar un format sense "
"pèrdua com PNG o TIFF per a emmagatzemar les fotografies. En cas contrari, "
"perdreu una mica cada cop que deseu el resultat. Si us ho podeu permetre, "
"utilitzeu 16 bits/canal per a les vostres millors captures. Si voleu canviar "
"l'espai de color, en particular si voleu treballar amb CIEL*a*b, es "
"requereixen 16 bits per canal per a evitar efectes de truncament."

#: ../../image_editor/workflow_tools.rst:43
msgid ""
"If you swap step 4. and 5., which is well possible if the initial contrast "
"is already very good, you can do the first 4. adjustment in the same tool, "
"the **White Balance**."
msgstr ""
"Si canvieu els pasos 4 i 5, el qual és molt possible si el contrast inicial "
"ja és molt bo, podreu realitzar el primer ajust 4 en la mateixa eina, el "
"**Balanç de blancs**."

#: ../../image_editor/workflow_tools.rst:45
msgid ""
"Many of the tools you may need for photographic editing are included with "
"digiKam. Their description follows here."
msgstr ""
"Moltes de les eines que podreu necessitar per a l'edició fotogràfica "
"s'inclouen amb el digiKam. La seva descripció segueix aquí."

#: ../../image_editor/workflow_tools.rst:48
msgid "Common Editing Tools Features"
msgstr "Característiques comunes de les eines d'edició"

#: ../../image_editor/workflow_tools.rst:50
msgid ""
"All Image Editor tools like Sharpen, Blur, Noise Reduction, Refocus, Unsharp "
"Mask, etc. use a common dialog style that previews the effect before "
"applying the filter to the current image. Below you see the **Solarize** "
"tool in action using this common dialog layout:"
msgstr ""
"Totes les eines de l'editor d'imatges com ara Aguditza, Difumina, Reducció "
"del soroll, Reenfoca, Màscara de desenfocament, etc. utilitzen un estil de "
"diàleg comú que mostra una vista prèvia de l'efecte abans d'aplicar el "
"filtre a la imatge actual. A continuació, veureu l'eina **Solaritza** en "
"acció utilitzant aquesta disposició de diàleg comú:"

#: ../../image_editor/workflow_tools.rst:56
msgid "The Solarize Effect is a Common Tool to Edit Photograph"
msgstr "L'efecte Solaritza és una eina habitual per a editar les fotografies"

#: ../../image_editor/workflow_tools.rst:58
msgid "See the list of common areas available in all tools:"
msgstr "Consulteu la llista d'àrees comunes disponibles en totes les eines:"

#: ../../image_editor/workflow_tools.rst:60
msgid ""
"(1): The seven buttons to the top left select the comparison style of the "
"preview area. The modes are:"
msgstr ""
"(1): Els set botons a la part superior esquerra seleccionen l'estil de "
"comparació de l'àrea de vista prèvia. Els modes són:"

#: ../../image_editor/workflow_tools.rst:62
msgid "**Original** image."
msgstr "Imatge **original**."

#: ../../image_editor/workflow_tools.rst:64
msgid ""
"**Split vertically without duplication**: The left area shows the original "
"whereas the right side shows the filter applied to the continuation of the "
"selected zoom."
msgstr ""
"**Divideix verticalment sense duplicacions**: l'àrea de l'esquerra mostra "
"l'original, mentre que la del costat dret mostra el filtre aplicat a la "
"continuació del zoom seleccionat."

#: ../../image_editor/workflow_tools.rst:66
msgid ""
"**Split horizontally without duplication**: The top area shows the original "
"whereas the lower area shows the filter applied to the continuation of the "
"selected zoom."
msgstr ""
"**Divideix horitzontalment sense duplicacions**: l'àrea de dalt mostra "
"l'original, mentre que la de l'esquerra mostra el filtre aplicat a la "
"continuació del zoom seleccionat."

#: ../../image_editor/workflow_tools.rst:68
msgid ""
"**Split vertically**: The left area shows the original and the right one the "
"filter effect for comparison."
msgstr ""
"**Divideix verticalment**: l'àrea de l'esquerra mostra l'original i la de la "
"dreta l'efecte del filtre per a comparar."

#: ../../image_editor/workflow_tools.rst:70
msgid ""
"**Split horizontally**: The top area shows the original and the lower one "
"the filter effect for comparison."
msgstr ""
"**Divideix horitzontalment**: l'àrea de dalt mostra l'original i la de sota "
"l'efecte del filtre per a comparar."

#: ../../image_editor/workflow_tools.rst:72
msgid "**Effect preview**: This is a live effect preview without comparison."
msgstr ""
"**Vista prèvia dels efectes**: aquesta és una vista prèvia directa de "
"l'efecte, sense comparació."

#: ../../image_editor/workflow_tools.rst:74
msgid ""
"**Mouse over** style: preview when mouse is off the preview (in the settings "
"area), otherwise shows original."
msgstr ""
"Estil **passar el ratolí**: vista prèvia quan el ratolí es troba fora de la "
"vista prèvia (a l'àrea de configuració), altrament mostra l'original."

#: ../../image_editor/workflow_tools.rst:76
msgid ""
"(2): There are two buttons that can be toggled to show **over-exposure** or "
"**under-exposure**. The colors can be customized in the :ref:`Editor Window "
"Settings <editor_settings>` from Setup dialog."
msgstr ""
"(2): Hi ha dos botons que es poden alternar per a mostrar **sobreexposició** "
"o **subexposició**. Els colors es poden personalitzar a la :ref:"
"`configuració de la finestra de l'editor <editor_settings>` des del diàleg "
"Configuració."

#: ../../image_editor/workflow_tools.rst:78
msgid ""
"(3): The **preview** area. Click and drag with the mouse to move it on the "
"image. The preview area on the left of dialog is updated accordingly."
msgstr ""
"(3): L'àrea de **vista prèvia**. Feu-hi clic i arrossegueu amb el ratolí per "
"a moure'l sobre la imatge. L'àrea de vista prèvia a l'esquerra del diàleg "
"s'actualitzarà en conseqüència."

#: ../../image_editor/workflow_tools.rst:80
msgid "(4): The filter or tool **settings** area."
msgstr "(4): L'àrea de **configuració** del filtre o eina."

#: ../../image_editor/workflow_tools.rst:82
msgid ""
"(5): The **zoom** slider and buttons to change the canvas and preview zoom "
"level."
msgstr ""
"(5): El control lliscant de **zoom** i els botons per a canviar el llenç i "
"obtenir una vista prèvia amb el nivell de zoom."

#: ../../image_editor/workflow_tools.rst:84
msgid ""
"(6): There is a **progress indicator** for tool with involved calculations "
"at processing stage, else the current **file-name** is displayed."
msgstr ""
"(6): Hi ha un **indicador de progrés** per a l'eina amb càlculs involucrats "
"a l'etapa de processament, en cas contrari, es mostrarà el nom del fitxer "
"actual."

#: ../../image_editor/workflow_tools.rst:86
msgid ""
"The Image Editor tools are available as plugins loaded dynamically at "
"startup and configurable in :ref:`Setup dialog <plugins_settings>`. List of "
"tools is given below:"
msgstr ""
"Les eines de l'editor d'imatges estan disponibles com a connectors que es "
"carreguen dinàmicament a l'inici i que es poden configurar en el :ref:"
"`diàleg de Configuració <plugins_settings>`. La llista d'eines s'indica a "
"continuació:"

#: ../../image_editor/workflow_tools.rst:88
msgid "Image Color Corrections:"
msgstr "Correccions del color de la imatge:"

#: ../../image_editor/workflow_tools.rst:90
msgid ":ref:`Encoding Depth <color_depth>`"
msgstr ":ref:`Profunditat de codificació <color_depth>`"

#: ../../image_editor/workflow_tools.rst:92
msgid ":ref:`Color-Space Converter <color_cm>`"
msgstr ":ref:`Convertidor de l'espai de color <color_cm>`"

#: ../../image_editor/workflow_tools.rst:94
msgid ":ref:`Auto Correction <color_auto>`"
msgstr ":ref:`Correcció automàtica del color <color_auto>`"

#: ../../image_editor/workflow_tools.rst:96
msgid ":ref:`Brightness / Contrast / Gamma <color_bcg>`"
msgstr ":ref:`Brillantor / Contrast / Gamma <color_bcg>`"

#: ../../image_editor/workflow_tools.rst:98
msgid ":ref:`Hue / Saturation / Lightness <color_hsl>`"
msgstr ":ref:`To / Saturació / Claredat <color_hsl>`"

#: ../../image_editor/workflow_tools.rst:100
msgid ":ref:`Colors Balance <color_balance>`"
msgstr ":ref:`Equilibri de color <color_balance>`"

#: ../../image_editor/workflow_tools.rst:102
msgid ":ref:`Adjust Levels <color_levels>`"
msgstr ":ref:`Ajust dels nivells <color_levels>`"

#: ../../image_editor/workflow_tools.rst:104
msgid ":ref:`White Balance <color_wb>`"
msgstr ":ref:`Balanç de blancs <color_wb>`"

#: ../../image_editor/workflow_tools.rst:106
msgid ":ref:`Curves Adjust <color_curves>`"
msgstr ":ref:`Ajust de les corbes <color_curves>`"

#: ../../image_editor/workflow_tools.rst:108
msgid ":ref:`Channels Mixer <color_mixer>`"
msgstr ":ref:`Mesclador de canals <color_mixer>`"

#: ../../image_editor/workflow_tools.rst:110
msgid ":ref:`Black and White <color_bw>`"
msgstr ":ref:`Blanc i negre <color_bw>`"

#: ../../image_editor/workflow_tools.rst:112
msgid ":ref:`Simulate Infrared Film <color_infrared>`"
msgstr ":ref:`Simular pel·lícula infraroja <color_infrared>`"

#: ../../image_editor/workflow_tools.rst:114
msgid ":ref:`Simulate Negative Film <color_negative>`"
msgstr ":ref:`Simular una pel·lícula en negatiu <color_negative>`"

#: ../../image_editor/workflow_tools.rst:116
msgid ":ref:`Invert Colors <color_invert>`"
msgstr ":ref:`Inverteix els colors <color_invert>`"

#: ../../image_editor/workflow_tools.rst:118
msgid "Image Enhancement:"
msgstr "Millorar la imatge:"

#: ../../image_editor/workflow_tools.rst:120
msgid ":ref:`Sharpening (sharpen, unsharp mask, refocus) <enhance_sharpen>`"
msgstr ""
":ref:`Aguditzar (aguditzar, màscara de desenfocament, reenfocar) "
"<enhance_sharpen>`"

#: ../../image_editor/workflow_tools.rst:122
msgid ":ref:`Blur Tool <enhance_blur>`"
msgstr ":ref:`Eina de difuminat <enhance_blur>`"

#: ../../image_editor/workflow_tools.rst:124
msgid ":ref:`Red Eyes Removal <enhance_redeyes>`"
msgstr ":ref:`Eliminar els ulls vermells <enhance_redeyes>`"

#: ../../image_editor/workflow_tools.rst:126
msgid ":ref:`Local Contrast <enhance_localcontrast>`"
msgstr ":ref:`Contrast local <enhance_localcontrast>`"

#: ../../image_editor/workflow_tools.rst:128
msgid ":ref:`Noise Reduction <enhance_nr>`"
msgstr ":ref:`Reducció del soroll <enhance_nr>`"

#: ../../image_editor/workflow_tools.rst:130
msgid ":ref:`Hot Pixel Correction <enhance_hotpixels>`"
msgstr ":ref:`Correcció dels píxels cremats <enhance_hotpixels>`"

#: ../../image_editor/workflow_tools.rst:132
msgid ":ref:`Lens Distortion Correction <enhance_lensdistortion>`"
msgstr ""
":ref:`Corregeix automàticament la distorsió de la lent "
"<enhance_lensdistortion>`"

#: ../../image_editor/workflow_tools.rst:134
msgid ":ref:`Photograph Restoration <enhance_restoration>`"
msgstr ":ref:`Restauració fotogràfica <enhance_restoration>`"

#: ../../image_editor/workflow_tools.rst:136
msgid ":ref:`Vignetting Correction Tool <enhance_vignetting>`"
msgstr ":ref:`Eina de correcció del vinyetatge <enhance_vignetting>`"

#: ../../image_editor/workflow_tools.rst:138
msgid ":ref:`Lens Auto-Correction Tool <enhance_lensauto>`"
msgstr ":ref:`Correcció automàtica de la lent <enhance_lensauto>`"

#: ../../image_editor/workflow_tools.rst:140
msgid ":ref:`Healing Clone Tool <enhance_clone>`"
msgstr ":ref:`Eina de clonatge de guariment <enhance_clone>`"

#: ../../image_editor/workflow_tools.rst:142
msgid "Image Transformation Tools:"
msgstr "Eines per a la transformació de les imatges:"

#: ../../image_editor/workflow_tools.rst:144
msgid ":ref:`Crop Tool <transform_crop>`"
msgstr ":ref:`Eina d'escapçat <transform_crop>`"

#: ../../image_editor/workflow_tools.rst:146
msgid ":ref:`Change Size <transform_resize>`"
msgstr ":ref:`Canviar la mida <transform_resize>`"

#: ../../image_editor/workflow_tools.rst:148
msgid ":ref:`Liquid Rescale <transform_liquidrescale>`"
msgstr ":ref:`Canvi d'escala líquid <transform_liquidrescale>`"

#: ../../image_editor/workflow_tools.rst:150
msgid ":ref:`Free Rotation <transform_freerotation>`"
msgstr ":ref:`Gir lliure <transform_freerotation>`"

#: ../../image_editor/workflow_tools.rst:152
msgid ":ref:`Perspective Adjustment <transform_perspective>`"
msgstr ":ref:`Ajustament de la perspectiva <transform_perspective>`"

#: ../../image_editor/workflow_tools.rst:154
msgid ":ref:`Shearing Tool <transform_shear>`"
msgstr ":ref:`Eina d'inclinació <transform_shear>`"

#: ../../image_editor/workflow_tools.rst:156
msgid "Image Decoration Tools:"
msgstr "Eines per a la decoració de les imatges:"

#: ../../image_editor/workflow_tools.rst:158
msgid ":ref:`Add Border <decorate_border>`"
msgstr ":ref:`Afegeix una vora <decorate_border>`"

#: ../../image_editor/workflow_tools.rst:160
msgid ":ref:`Insert Text <decorate_inserttext>`"
msgstr ":ref:`Insereix un text <decorate_inserttext>`"

#: ../../image_editor/workflow_tools.rst:162
msgid ":ref:`Apply Texture <decorate_texture>`"
msgstr ":ref:`Aplica una textura <decorate_texture>`"

#: ../../image_editor/workflow_tools.rst:164
msgid "Special Effect Filters:"
msgstr "Filtres per als efectes especials:"

#: ../../image_editor/workflow_tools.rst:166
msgid ":ref:`Color Effects <effects_color>`"
msgstr ":ref:`Efectes de color <effects_color>`"

#: ../../image_editor/workflow_tools.rst:168
msgid ":ref:`Add Film Grain <effects_filmgrain>`"
msgstr ":ref:`Afegeix gra de pel·lícula <effects_filmgrain>`"

#: ../../image_editor/workflow_tools.rst:170
msgid ":ref:`Simulate Oil Painting <effects_oilpaint>`"
msgstr ":ref:`Simula la pintura a l'oli <effects_oilpaint>`"

#: ../../image_editor/workflow_tools.rst:172
msgid ":ref:`Simulate Charcoal Drawing <effects_charcoal>`"
msgstr ":ref:`Simula un dibuix al carbonet <effects_charcoal>`"

#: ../../image_editor/workflow_tools.rst:174
msgid ":ref:`Emboss Photograph <effects_emboss>`"
msgstr ":ref:`Fotografia amb relleu <effects_emboss>`"

#: ../../image_editor/workflow_tools.rst:176
msgid ":ref:`Distortion FX <effects_distortion>`"
msgstr ":ref:`Efectes de distorsió <effects_distortion>`"

#: ../../image_editor/workflow_tools.rst:178
msgid ":ref:`Blur FX <effects_blur>`"
msgstr ":ref:`Efectes especials de difuminat <effects_blur>`"

#: ../../image_editor/workflow_tools.rst:180
msgid ":ref:`Add Rain Drops <effects_raindrops>`"
msgstr ":ref:`Afegeix gotes de pluja <effects_raindrops>`"

#: ../../image_editor/workflow_tools.rst:184
msgid ""
"All these tools are also available into the **Tools** tab from the Image "
"Editor right sidebar."
msgstr ""
"Totes aquestes eines també estan disponibles a la pestanya **Eines** de la "
"barra lateral dreta de l'editor d'imatges."

#: ../../image_editor/workflow_tools.rst:190
msgid "The digiKam Image Editor Tools Tab from the Right Sidebar"
msgstr ""
"La pestanya Eines des de la barra lateral dreta en l'editor d'imatges del "
"digiKam"

#: ../../image_editor/workflow_tools.rst:195
msgid "Raw Workflow"
msgstr "Flux de treball RAW"

#: ../../image_editor/workflow_tools.rst:197
msgid "A typical RAW workflow with digiKam may follow these steps:"
msgstr "Un flux de treball RAW típic amb el digiKam pot seguir aquests passos:"

#: ../../image_editor/workflow_tools.rst:199
msgid "Setting up the RAW conversion and color management preferences:"
msgstr "Configurar la conversió RAW i les preferències de la gestió del color:"

#: ../../image_editor/workflow_tools.rst:201
msgid "Get the ICC color profiles for the camera, display and printer."
msgstr ""
"Obtenir els perfils de color ICC per a la càmera, la pantalla i la "
"impressora."

#: ../../image_editor/workflow_tools.rst:203
msgid "Setup digiKam correctly defining a color work space."
msgstr ""
"Configurar el digiKam correctament per a definir un espai de treball de "
"color."

#: ../../image_editor/workflow_tools.rst:205
msgid "Camera whitepoint setting."
msgstr "Ajustar el punt blanc de la càmera."

#: ../../image_editor/workflow_tools.rst:207
msgid ""
"Demosaicing (includes interpolation, noise filtering and chromatic "
"aberration correction)."
msgstr ""
"Reconstrucció de color (inclou la interpolació, filtratge del soroll i la "
"correcció de l'aberració cromàtica)."

#: ../../image_editor/workflow_tools.rst:209
msgid "Conversion to a color space."
msgstr "Conversió a un espai de color."

#: ../../image_editor/workflow_tools.rst:211
msgid "Gamma correction, tone mapping."
msgstr "Correcció de la gamma, mapatge dels tons."

#: ../../image_editor/workflow_tools.rst:213
msgid "Color profile assignment."
msgstr "Assignar perfils de color."

#: ../../image_editor/workflow_tools.rst:215
msgid "Dynamic range expansion (optional)."
msgstr "Marge dinàmic expandit (opcional)."

#: ../../image_editor/workflow_tools.rst:217
msgid "Continue with :ref:`standard workflow <standard_workflow>`."
msgstr "Continueu amb el :ref:`flux de treball estàndard <standard_workflow>`."

#: ../../image_editor/workflow_tools.rst:219
msgid ""
"From there everything is simple, select a RAW image and open it in the "
"editor. The Raw Import tool will be displayed after the image has been "
"processed in order to adjust color rendering. Without color management all "
"images are very dark, this is normal. You can experiment the various "
"profiles you have in order to select the best input profile according to "
"your image (some tends to be very neutral, even a bit dull, some gives more "
"saturated results, etc.)."
msgstr ""
"A partir d'aquí tot és senzill, seleccioneu una imatge RAW i obriu-la a "
"l'editor. L'eina d'importació RAW es mostrarà després que la imatge hagi "
"estat processada per a ajustar la reproducció del color. Sense gestió del "
"color, totes les imatges seran molt fosques, això és normal. Podreu "
"experimentar amb els diferents perfils que teniu per a seleccionar el millor "
"perfil d'entrada segons la vostra imatge (alguns tendeixen a ser molt "
"neutrals, fins i tot una mica avorrits, alguns donen un resultat més "
"saturat, etc.)."

#: ../../image_editor/workflow_tools.rst:225
msgid "The RAW Import Tool Allow to Load Digital Negative in Image Editor"
msgstr ""
"L'eina d'importació RAW permet carregar negatius digitals en l'editor "
"d'imatges"
